/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin17_2;

/**
 *
 * @author BeJa
 */
public class Metodos {
/**
 * Metodo que asina notas (enteros entre 1 y 10 a un array determinado
 * @param array introducimos el array al que queremos asignar valores
 */
    public static void asignaNota(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 11);
            System.out.println("nota "+i+ " = "+array[i]);
        }
    }
/**
 * Metodo que calcula el numero de aprobados y suspensos
 * @param array introducimos array con una lista de notas
 */
    public static void verAproSusp(int[] array) {
        int apro = 0, susp = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 5) {
                apro = apro + 1;
            } else {
                susp = susp + 1;
            }
        }
        System.out.println("Aprobados: " + apro + "\n" + "Suspensos: " + susp);
    }
/**
 * Calula la nota media de una lista de notas
 * @param array introducimos array con una lista de notas
 */
    public static void notaMedia(int[] array) {
        int  media=0,suma=0;
        for (int i = 0; i < array.length; i++) {
            suma=suma+array[i];
            //media = media + (array[i] / array.length)
                    ;
        }
        media=suma/array.length;
        System.out.println("A nota media é " + media);

    }
/**
 * Calcula la nota mas alta de un array de notas
 * @param array introducimos array con una lista de notas
 */
    public static void notaAlta(int[] array) {
        int alta = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > alta) {
                alta = array[i];
            }

        }
        System.out.println("A nota mais alta é " + alta);
    }

}
    

