package boletin17_2;

/**
 *
 * @author rsanromanlorenzo
 * @version v1.0
 */
/*
 2- Carga un array  de tipo int, cas notas no módulo de programación ,dos 30 alumnos da clase de DAM .
 •	 Visualiza o numero de aprobados e o de suspensos  .
 •	 Calcula e visualiza a nota media da clase
 •	Visualiza a nota mais alta .

 */
//Prueba tras tag
public class BOLETIN17_2 {

    public static void main(String[] args) {
        int[] notas = new int[30];//creo array de notas

        Metodos.asignaNota(notas);//asigno valores al array notas
        Metodos.verAproSusp(notas);//visualizo cantidad de aprobados y suspensos
        Metodos.notaMedia(notas);//visualizo nota media
        Metodos.notaAlta(notas);//visualizo nota mas alta

    }
}
